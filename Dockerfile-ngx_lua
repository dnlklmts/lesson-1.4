FROM debian:9 as builder

RUN apt update && apt install -y \
    gcc \
    libc6-dev \
    libpcre3-dev \
    libssl-dev \
    make \
    wget \
    zlib1g-dev
RUN wget "https://github.com/openresty/luajit2/archive/v2.1-20200102.tar.gz" \
  && tar xvfz ./v2.1-20200102.tar.gz \
  && cd ./luajit2-2.1-20200102 \
  && make \
  && make install \
  && cd ..
RUN wget "https://github.com/vision5/ngx_devel_kit/archive/v0.3.1.tar.gz" \
    -O ./ngx-devel-kit-v0.3.1.tar.gz \
  && tar xvfz ./ngx-devel-kit-v0.3.1.tar.gz
RUN wget "https://github.com/openresty/lua-nginx-module/archive/v0.10.17.tar.gz" \
    -O ./lua-nginx-module-v0.10.17.tar.gz \
  && tar xvfz ./lua-nginx-module-v0.10.17.tar.gz
RUN wget "https://github.com/openresty/lua-resty-core/archive/v0.1.19.tar.gz" \
    -O ./lua-resty-core-v0.1.19.tar.gz \
  && tar xvfz ./lua-resty-core-v0.1.19.tar.gz \
  && mkdir /usr/local/lib/lua-resty-core \
  && cp -r ./lua-resty-core-0.1.19/lib /usr/local/lib/lua-resty-core/
RUN wget "https://github.com/openresty/lua-resty-lrucache/archive/v0.10.tar.gz" \
    -O ./lua-resty-lrucache-v0.10.tar.gz \
  && tar xvfz ./lua-resty-lrucache-v0.10.tar.gz \
  && mkdir /usr/local/lib/lua-resty-lrucache \
  && cp -r ./lua-resty-lrucache-0.10/lib/resty /usr/local/lib/lua-resty-lrucache/
RUN wget "https://nginx.org/download/nginx-1.18.0.tar.gz" \
    -O ./nginx-1.18.0.tar.gz \
  && tar xvfz ./nginx-1.18.0.tar.gz \
  && cd ./nginx-1.18.0 \
  && export LUAJIT_LIB=/usr/local/lib \
  && export LUAJIT_INC=/usr/local/include/luajit-2.1 \
  && ./configure \
      --with-ld-opt="-Wl,-rpath,/usr/local/lib" \
      --add-module=../ngx_devel_kit-0.3.1 \
      --add-module=../lua-nginx-module-0.10.17 \
  && make \
  && make install

FROM debian:9
WORKDIR /usr/local/nginx/sbin
COPY --from=builder /usr/local/nginx/sbin/nginx .
COPY --from=builder /usr/local/lib/libluajit-5.1.so.2.1.0 /usr/local/lib/libluajit-5.1.so.2
COPY --from=builder /usr/local/lib/lua-resty-core /usr/local/lib/lua-resty-core
COPY --from=builder /usr/local/lib/lua-resty-lrucache /usr/local/lib/lua-resty-lrucache
RUN mkdir ../logs ../conf \
  && touch ../logs/error.log ../logs/access.log \
  && chmod +x ./nginx
CMD  ["./nginx", "-g", "daemon off;"]
