# Less1.4: Docker multi-stage builds

1. Необходимо собрать nginx из исходников со стандартными модулями и запустить бинарник во втором образе. Образы Debian9. При запуске примаунтить конфиг в контейнер.
    * `$ docker build -t nginx:latest -f ./Dockerfile-nginx .`
    * `$ docker run -d -ti --name=tiny_nginx -v "$(pwd)"/nginx.conf:/usr/local/nginx/conf/nginx.conf -p 8001:8081 nginx:latest`

2. Добавить в сборку <https://github.com/openresty/lua-nginx-module>.
    * С использованием `openresty` бандла: 
        * `$ docker build -t openresty:latest -f ./Dockerfile-openresty .`
        * `$ docker run -d -ti --name=openresty_nginx -v "$(pwd)"/ngx_lua.conf:/usr/local/openresty/nginx/conf/nginx.conf -p 8002:8081 openresty:latest`
    * Ручная сборка `ngx_lua` с Nginx:
        * `$ docker build -t mynginx:latest -f ./Dockerfile-ngx_lua .`
        * `$ docker run -d -ti --name=with_ngx_lua -v "$(pwd)"/ngx_lua.conf:/usr/local/nginx/conf/nginx.conf -p 8003:8081 mynginx:latest`
